# Overview
<!-- ## This is test sub heading 3 -->
The best way to reach any goal is to start with a solid plan. In the world of software, that plan is encompassed in the SDLC: Software Development Life Cycle. Viewed through the lens of a Software Engineer, we break the SDLC into these phases:

-  **Design**: Refining the Product and breaking it down into discrete units of work
-  **Develop**: Turning those units of work into a software Product
-  **Test**: Validating the Product
-  **Deploy** and Release: Making the Product available to our customers
-  **Manage**: Ensuring our Product continues to delight our customers

Here, we will focus on the Design phase, but feel free to explore the other phases via the Lifecycle menu above.

# Processes
There is one over-arching process that governs the Design phase: The Working Model.

##The Working Model
The Working Model helps teams refine, decompose, assign, and deliver the Product. You will also hear this process referred to as “CARBL”, which is an acronym for the Working Model’s phases:

- **Concept**: Refining the Product intend to build
- **Assessment**: Reviewing the Concept and proposed software design/architecture
- **Requirements**: Decomposing the Product into discrete units of work
-  **Build**: Taking those units of work and writing code to implement them
-  **Launch**: Releasing the Product for our customers to consume

Most Software Engineers at T-Mobile will primarily work in the Build and Launch phases, so here we will refer to the first three Working Model Phases – Concept, Assessment, and Requirements – as the “Design” phase.

Tools
One key benefit of decomposing our Products into smaller units of work is that by using configuration, feature flags, etc, we can discretely deliver this functionality – rather than wait on a “Big Bang” release. Since most Products have associated timelines we must meet (e.g.: introduction of the latest Apple or Samsung phone), it is critical that we be able to track progress toward completion of the Product.

There are two primary tools that allow us to do this: Jira and Jira Align.

Jira is where User Stories are managed and where Software Engineers (SWEs) will track the status of their work items. There are pre-built integrations with our CICD platform that allow Jira tickets to be updated as SWEs commit code to implement those User Stories.

Jira Align automatically synchronizes with Jira to allow reporting across an entire Product’s development. This allows SWEs to focus on their User Stories and still give Management the vision into progress they need to run the business.

You will find access to both Jira and Jira Align by  logging into Okta.

